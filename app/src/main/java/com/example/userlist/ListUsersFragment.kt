package com.example.userlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*

class ListUsersFragment :Fragment(){
    private var userList: MutableList<LoginUserFragment.Users>? = null
    private var mrecylerview : RecyclerView? = null
    lateinit var ref: DatabaseReference
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.listuser_fragment,container,false)

        userList = mutableListOf()
        mrecylerview = view.findViewById(R.id.recyclerView)
        mrecylerview?.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL,false)

        ref = FirebaseDatabase.getInstance().getReference("Users")

        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
               Toast.makeText(context, "Error Occurred "+ p0.toException(), Toast.LENGTH_SHORT).show()

            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    for (h in p0.children) {
                        val user = h.getValue(LoginUserFragment.Users::class.java)
                        userList?.add(user!!)
                    }
                    val adapter = UserAdapter(context!!, userList!!)

                    mrecylerview?.adapter = adapter


                }
            }

        })
        return view
    }

}