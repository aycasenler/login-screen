package com.example.userlist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class UserAdapter(val context : Context, val userList: MutableList<LoginUserFragment.Users>) :
    RecyclerView.Adapter<UserAdapter.MyViewHolder>() {
    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false)
        return MyViewHolder(itemView)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val user = userList[position]
        holder.txtName?.text = user.name
        holder.txtPass?.text = user.password


    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

         val txtName: TextView? = itemView.findViewById(R.id.txtName)
         val txtPass : TextView? = itemView.findViewById(R.id.txtPass)

    }
}
//class UserAdapter (var mcontext : Context, var layoutResId : Int, var userList : List<LoginUserFragment.Users>)
//    : ArrayAdapter<LoginUserFragment.Users>(mcontext,layoutResId, userList){
//    private var txtName : TextView? = null
//    private var txtPassword : TextView? = null
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
//       val layoutInflater : LayoutInflater = LayoutInflater.from(context)
//        val view : View = layoutInflater.inflate(layoutResId,null)
//
//         txtName = view.findViewById(R.id.txtName)
//        txtPassword = view.findViewById(R.id.txtPass)
//
//        val user = userList[position]
//
//        txtName?.text = user.name
//        txtPassword?.text = user.password
//
//        return view
//    }
//}