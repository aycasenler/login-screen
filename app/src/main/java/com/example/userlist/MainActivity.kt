package com.example.userlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
       // init()
        initFragment()

    }

        fun initFragment() {
        val fragmentLogin = LoginUserFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.frameLayout, fragmentLogin, "LoginUserFragment")
        transaction.addToBackStack("LoginUserFragment")
        transaction.commit()

    }
//
}
