package com.example.userlist

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.database.*

class LoginUserFragment : Fragment(), View.OnClickListener {

    lateinit var ref: DatabaseReference
    private var etName: EditText? = null
    private var etPassword: EditText? = null
    private var btnSave: Button? = null
    private var btnList: Button? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.loginuser_fragment, container, false)
        init(view)
        return view
    }

    fun init(view: View) {

        ref = FirebaseDatabase.getInstance().getReference("Users")
        etName = view.findViewById(R.id.etUserName)
        etPassword = view.findViewById(R.id.etPassword)
        btnSave = view.findViewById(R.id.btnSave)
        btnList = view.findViewById(R.id.btnList)

        btnSave?.setOnClickListener(this)
        btnList?.setOnClickListener(this)

    }

    fun initFragment() {
        val fragmentList = ListUsersFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, fragmentList, "ListUserFragment")
        transaction?.addToBackStack("ListUserFragment")
        transaction?.commit()

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btnSave?.id -> {
                saveData()
                Handler().postDelayed({
                    val name = etName?.text.toString().trim()
                    val password = etPassword?.text.toString().trim()
                    if(!name.isEmpty() && !password.isEmpty())
                        initFragment()
                }, 1000)

            }
            btnList?.id -> {

                initFragment()

            }
        }
    }

    fun saveData() {
        val name = etName?.text.toString().trim()
        if (name.isEmpty()) {
            //Toast.makeText(this,"Please enter a name!",Toast.LENGTH_SHORT).show()
            etName?.error = "Please enter a name!"
            return
        }
        val password = etPassword?.text.toString().trim()
        if (password.isEmpty()) {
            //Toast.makeText(this,"Please enter a name!",Toast.LENGTH_SHORT).show()
            etPassword?.error = "Please enter a password!"
            return
        }

        val userId = ref.push().key

        val users = Users(userId!!, name, password)

        ref.child(userId).setValue(users).addOnCompleteListener {
            Toast.makeText(context, "User saved successfully", Toast.LENGTH_SHORT).show()
        }
    }

    data class Users(
        var id: String = "",
        val name: String = "",
        val password: String = ""

    )

}